/**
 * 
 */
package buclesejs.bucles1;

import java.util.Scanner;

/**
 * @author Asgarth
 *
 */
public class DrawSquare {

	private static Scanner readFromConsole;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		readFromConsole = new Scanner(System.in);
		String line = "";
		Integer numero, i, k;
		
		System.out.println("Un numero entero:");
		line = readFromConsole.nextLine();
		numero = Integer.parseInt(line);
		
		for (i = 0 ; i < numero ; i++){
			System.out.println("");
			for (k = 0 ; k < numero; k++){
				System.out.print("*");
			
		}
		}
		
	}
	}