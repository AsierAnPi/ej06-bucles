/**
 * 
 */
package buclesejs.bucles1;

import java.util.Scanner;

/**
 * @author Asgarth
 *
 */
public class Factorial {

	private static Scanner readFromConsole;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		readFromConsole = new Scanner(System.in);
		String line = "";
		Integer numero, i, resultado;
		resultado = 1;
		
		System.out.println("Un numero entero:");
		line = readFromConsole.nextLine();
		numero = Integer.parseInt(line);
		
		for (i = 1 ; i-1 < numero ; i++){
		resultado = resultado * i;
		
		}
		System.out.println(resultado);
	}
	}